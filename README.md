# Belajar Git #

1. Membuat repository lokal

    ```
    git init
    ```

2. Melihat status folder lokal

    ```
    git status
    ```

    Outputnya

    ```
    On branch master

    No commits yet

    Untracked files:
    (use "git add <file>..." to include in what will be committed)
        .DS_Store
        README.md
    ```

3. Menambah file/perubahan ke `staging area`

    ```
    git add namafile
    ```

    Setelah itu cek lagi dengan `git status`

    ```
    On branch master

    No commits yet

    Changes to be committed:
    (use "git rm --cached <file>..." to unstage)
        new file:   README.md

    Untracked files:
    (use "git add <file>..." to include in what will be committed)
        .DS_Store
    ```

4. Simpan semua perubahan di staging area

    ```
    git commit
    ```

    Bila tidak mau menggunakan text editor, kita bisa pakai opsi `-m`

    ```
    git commit -m "memperbaiki bug di baris 60"
    ```

5. Melihat riwayat perubahan

    ```
    git log
    ```

    Outputnya seperti ini 

    ```
    commit 408cb4eebc4b9bbc6a395cbf727dadfe58993e25 (HEAD -> master)
    Author: Endy Muhardin <endy.muhardin@gmail.com>
    Date:   Thu Dec 9 14:27:41 2021 +0700

        perintah dasar git

    commit 4e234e3a91a7bbcae32907c9240623ae26ebaf32
    Author: Endy Muhardin <endy.muhardin@gmail.com>
    Date:   Thu Dec 9 14:22:19 2021 +0700

        commit pertama
    ```

6. Menambahkan repository di remote

    ```
    git remote add namaremote urlremote
    ```

    Contoh

    ```
    git remote add gitlab git@gitlab.com:training-askrindo-202101/belajar-git.git
    ```

7. Upload / push perubahan di repo lokal ke remote `gitlab`

    ```
    git push namaremote namabranch
    ```

    Contoh 
    
    ```
    git push gitlab master
    ```